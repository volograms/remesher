#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include <boost/program_options.hpp>

#include "libremesh/modelwriter.h"
#include "libremesh/defines.h"
#include "libremesh/hrtimer.h"
#include "libremesh/interface.h"
#include "libremesh/vec3.h"

#if 1
///* Beethoven settings */
// # define MESH_VERTS 500000
// # define LLOYD_ITER 100
// # define USE_DENSITY_FIELD true
// # define DENSITY_MAX_CURVATURE 8000
// # define DENSITY_CONTRAST_EXP 3.0f
// # define DENSITY_SMOOTH_ITER 50

// Becket play settings
// # define MESH_VERTS 100000
//# define MESH_VERTS 40000
//# define LLOYD_ITER 5
//# define USE_DENSITY_FIELD true
//# define DENSITY_MAX_CURVATURE 500
//# define DENSITY_CONTRAST_EXP 2.0f
//# define DENSITY_SMOOTH_ITER 50

#endif

//#if 0
///* Bunny settings. */
//# define MESH_VERTS 20000
//# define LLOYD_ITER 100
//# define USE_DENSITY_FIELD true
//# define DENSITY_MAX_CURVATURE 500
//# define DENSITY_CONTRAST_EXP 2.0f
//# define DENSITY_SMOOTH_ITER 20
//#endif

int main (int argc, char** argv)
{


    int MESH_VERTS;
    int LLOYD_ITER;
    bool USE_DENSITY_FIELD;
    int DENSITY_MAX_CURVATURE;
    float DENSITY_CONTRAST_EXP;
    int DENSITY_SMOOTH_ITER;

    std::string infile, outfile;

    // Parsing execution options
    try {

        namespace po = boost::program_options;

        po::options_description desc("Options");
        desc.add_options()
                ("help,h", "Print this help message")
                ("input,i", po::value<std::string>(&infile)->required(), "Input mesh")
                ("output,o", po::value<std::string>(&outfile)->required(), "Output mesh")
                ("vertices,v", po::value<int>(&MESH_VERTS)->default_value(40000), "Number of vertices in the output mesh")
                ("lloyd-iter,l", po::value<int>(&LLOYD_ITER)->default_value(5), "Number of Lloyd iterations")
                ("use-density-field,d", po::value<bool>(&USE_DENSITY_FIELD)->default_value(true), "Use density field")
                ("density-max-curvature", po::value<int>(&DENSITY_MAX_CURVATURE)->default_value(500), "Density max curvature")
                ("density-contrast-exp", po::value<float>(&DENSITY_CONTRAST_EXP)->default_value(2.0f), "Density contrast exp")
                ("density-smooth-iter", po::value<int>(&DENSITY_SMOOTH_ITER)->default_value(50), "Density smooth iterations")
                ;

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);


        if (vm.count("help")) {
            std::cerr << desc << std::endl;
            return 0;
        }

        po::notify(vm);

    }

    catch(std::exception& e) {
        std::cerr << "error: " << e.what() << std::endl;
        std::cerr << "Run: " << argv[0] << " -h     if you need help" << std::endl;
        return 1;
    }

    catch(...) {
        std::cerr << "Exception of unknown type!" << std:: endl;
    }


    Remesher::HRTimer t_total;

    std::size_t cleaning_time = 0;
    std::size_t density_time = 0;
    std::size_t resampling_time = 0;
    std::size_t lloyd_time = 0;
    double scale = 1.0;
    Remesher::Vec3f center;

    // Loading, cleaning
    Remesher::Interface iface;
    {
        iface.load_model(infile);
        Remesher::HRTimer t_cleaning;
        iface.get_reference_mesh()->scale_and_center();
        iface.clean_reference_mesh();
        iface.optimize_reference_mesh();
        cleaning_time = t_cleaning.get_elapsed();

        Remesher::TriangleMeshPtr mesh = iface.get_reference_mesh();
        scale = mesh->getScaleTransform();
        center = mesh->getCenterTransform();

        std::cout << "Reference mesh has " << mesh->get_vertices().size()
            << " vertices and " << mesh->get_faces().size() << " faces"
            << std::endl;
    }

    // Density field
    if (USE_DENSITY_FIELD)
    {
        Remesher::DensityFieldConf density_conf;
        density_conf.max_curvature = DENSITY_MAX_CURVATURE;
        density_conf.contrast_exp = DENSITY_CONTRAST_EXP;
        density_conf.smooth_iter = DENSITY_SMOOTH_ITER;
        iface.set_density_field_conf(density_conf);

        Remesher::HRTimer t_density;
        iface.exec_density_calculation();
        density_time = t_density.get_elapsed();
    }

    // Resampling
    {
        Remesher::ResamplingConf resampling_conf;
        resampling_conf.sample_amount = MESH_VERTS;
        resampling_conf.perform_decimation = true;
        iface.set_resampling_conf(resampling_conf);

        Remesher::HRTimer t_resampling;
        iface.exec_resampling();
        resampling_time = t_resampling.get_elapsed();
    }

    // Lloyd
    {
        Remesher::RelaxationConf lloyd_conf;
        lloyd_conf.iterations = LLOYD_ITER;
        iface.set_lloyd_conf(lloyd_conf);

        Remesher::HRTimer t_lloyd;
        iface.exec_lloyd();
        lloyd_time = t_lloyd.get_elapsed();
    }

    // Saving
    {
        Remesher::TriangleMeshPtr mesh_e(iface.get_evolving_mesh());
        mesh_e->rescale_and_recenter(scale, center);
        Remesher::ModelWriter::save_model(outfile, mesh_e);
    }

    std::stringstream ss;

    ss << "Cleaning took " << cleaning_time << "ms." << std::endl;
    ss << "Density took " << density_time << "ms." << std::endl;
    ss << "Resampling took " << resampling_time << "ms." << std::endl;
    ss << "Lloyd took " << lloyd_time << "ms (using "
        << (REMESHER_PARALLELIZATION * REMESHER_RELAXATION_THREADS)
        << " threads)" << std::endl;
    ss << "Total operation took " << t_total.get_elapsed()
        << "ms (" << (resampling_time + lloyd_time) << "ms "
        << "for resampling and relaxation)" << std::endl;

    std::cout << std::endl;
    std::cout << ss.str();
//    std::ofstream log((outfile + ".log").c_str());
//    log << ss.str();
//    log.close();

    return 0;
}
